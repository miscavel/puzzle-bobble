import { DEFAULT_WIDTH, DEFAULT_HEIGHT } from './Config';
import UIConfig from './UIConfig';

//Settings for texts
export default {
    score: {
        position: {x: DEFAULT_WIDTH / 2, y: UIConfig.segment.bubbleGroup.y / 2},
        style: {
            fontSize: '50px',
            fill: '#FFFFFF',
            fontFamily: 'Arial'
        }
    },
    fps: {
        position: {x: 20, y: DEFAULT_HEIGHT * 0.95},
        style: {
            fontSize: '30px',
            fill: '#FFFFFF'
        },
        text: 'fps: '
    },
    gameOver: {
        position: {x: DEFAULT_WIDTH / 2, y: DEFAULT_HEIGHT / 2 - 90},
        style: {
            fontSize: '50px',
            fill: '#FFFFFF',
            fontFamily: 'Arial'
        },
        text: 'Game Over'
    }
}
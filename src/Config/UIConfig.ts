import "phaser";
import { DEFAULT_WIDTH, DEFAULT_HEIGHT } from "./Config";
import { GameOverPanelSpriteConfig, RestartSpriteConfig } from './SpriteConfig';

export default {
    segment: {
        score: new Phaser.Math.Vector2(0, 0),
        bubbleGroup: new Phaser.Math.Vector2(0, 110),
        shooter: new Phaser.Math.Vector2(0, DEFAULT_HEIGHT * 0.8)
    },
    panel: {
        score: {
            position: new Phaser.Math.Vector2(DEFAULT_WIDTH / 2, 55),
            width: DEFAULT_WIDTH,
            height: 110,
            color: 0x314463
        },
        shooter: {
            position: new Phaser.Math.Vector2(DEFAULT_WIDTH / 2, DEFAULT_HEIGHT * 0.9),
            width: DEFAULT_WIDTH,
            height: DEFAULT_HEIGHT * 0.2,
            color: 0x7F6388
        },
        gameOver: {
            position: new Phaser.Math.Vector2(DEFAULT_WIDTH / 2, DEFAULT_HEIGHT / 2),
            name: GameOverPanelSpriteConfig.name
        }
    },
    restart: {
        position: new Phaser.Math.Vector2(DEFAULT_WIDTH / 2, DEFAULT_HEIGHT / 2 + 50),
        name: RestartSpriteConfig.name,
        originalScale: {x: 0.7, y: 0.7},
        hoverScale: {x: 0.9, y: 0.9}
    }
}
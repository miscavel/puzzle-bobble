//Config for animations

export const BubbleAnimationList = {
    pop: 'pop'
}

export const BubbleAnimationConfig = [
    {
        key: BubbleAnimationList.pop,
        frames: [0, 1, 2, 3, 4, 5],
        frameRate: 20,
        repeat: 0
    }
];
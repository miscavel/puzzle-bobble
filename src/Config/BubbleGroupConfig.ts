import BubbleConfig from './BubbleConfig';
import Constants from './Constants';
import UIConfig from './UIConfig';

//Config for bubble group
export default {
    initialRow: 7, //Number of rows to be generated in the beginning of the game
    totalRow: 13, //Total number of rows of slots in the game (how many rows of bubbles is allowed before the player dies)
    bedRow: 2, //Initial rows that cannot be popped (hidden)
    startingPosition:{
        x: [
            //Starting x-coordinate for the first bubble in the row
            BubbleConfig.bubble.radius, //Even row [0]
            BubbleConfig.bubble.diameter //Odd row [1]
        ],
        y: BubbleConfig.bubble.radius + UIConfig.segment.bubbleGroup.y - (Math.tan(60 * Constants.DEG_TO_RAD) * BubbleConfig.bubble.radius * 2), //Starting y-coordinate for the first row
    },
    columnCount: [
        //Number of bubbles in a row
        8, //Even row [0]
        7 //Odd row [1]
    ],
    increment: {
        x: BubbleConfig.bubble.diameter, //X-increment for the next bubble in a row
        y: Math.tan(60 * Constants.DEG_TO_RAD) * BubbleConfig.bubble.radius //Y-increment for the next row
    }
}
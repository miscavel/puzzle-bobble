import "phaser";
import { GameSceneKey } from '../../Config/SceneConfig';
import BubbleGroup from '../Object/BubbleGroup';
import Shooter from '../Object/Shooter';
import AudioConfig from '../../Config/AudioConfig';
import ScoreManager from '../Manager/ScoreManager';
import TextConfig from '../../Config/TextConfig';
import UIConfig from '../../Config/UIConfig';
import DepthConfig from '../../Config/DepthConfig';
import BubbleConfig from "../../Config/BubbleConfig";
import { DEFAULT_WIDTH, DEFAULT_HEIGHT } from "../../Config/Config";

export class GameScene extends Phaser.Scene {
    bubbleGroup: BubbleGroup;
    shooter: Shooter;
    score: ScoreManager;

    //UI
    fpsText: Phaser.GameObjects.Text;
    scorePanel: Phaser.GameObjects.Rectangle;
    gameOverPanel: Phaser.GameObjects.Image;
    restartButton: Phaser.GameObjects.Image;
    gameOverText: Phaser.GameObjects.Text;

    constructor() {
        super({
            key: GameSceneKey
        });
    }

    preload(): void {
        
    }

    create(): void {
        this.initializeObjects();
        this.initializeTexts();
        this.initializeControl();
        this.initializePhysics();
        this.initializeSounds();
    }

    initializeSounds(): void {
        this.sound.add(AudioConfig.pop.name);
    }
    

    initializeObjects(): void {
        //Score panel
        this.scorePanel = this.add.rectangle(UIConfig.panel.score.position.x, UIConfig.panel.score.position.y, UIConfig.panel.score.width, UIConfig.panel.score.height, UIConfig.panel.score.color);
        this.physics.add.existing(this.scorePanel);

        //Score manager & text
        this.score = ScoreManager.getInstance();
        this.score.setText(this.add.text(TextConfig.score.position.x, TextConfig.score.position.y, this.score.getScore() + '', TextConfig.score.style));
    
        //Shooter panel
        const shooterPanel = this.add.rectangle(UIConfig.panel.shooter.position.x, UIConfig.panel.shooter.position.y, UIConfig.panel.shooter.width, UIConfig.panel.shooter.height, UIConfig.panel.shooter.color);
        shooterPanel.setDepth(DepthConfig.shooterPanel);

        //BubbleGroup is the object responsible for handling bubbles
        this.bubbleGroup = new BubbleGroup(this);

        //Shooter
        this.shooter = new Shooter(this.bubbleGroup);

        //GameOver
        this.gameOverPanel = this.add.image(UIConfig.panel.gameOver.position.x, UIConfig.panel.gameOver.position.y, UIConfig.panel.gameOver.name);
        this.gameOverPanel.setDepth(DepthConfig.UI);
        this.gameOverPanel.setVisible(false);

        this.restartButton = this.add.image(UIConfig.restart.position.x, UIConfig.restart.position.y, UIConfig.restart.name);
        this.restartButton.setScale(UIConfig.restart.originalScale.x, UIConfig.restart.originalScale.y);
        this.restartButton.setDepth(DepthConfig.UI);
        this.restartButton.setVisible(false);
    }

    initializeTexts(): void {
       //Fps text
       this.fpsText = this.add.text(TextConfig.fps.position.x, TextConfig.fps.position.y, TextConfig.fps.text + Math.round(this.game.loop.actualFps), TextConfig.fps.style);
        
       //Update fps meter every second
       this.time.addEvent({
           delay: 1000,
           callback: function () {
               this.fpsText.setText(TextConfig.fps.text + Math.floor(this.game.loop.actualFps))
           }.bind(this),
           loop: true
       });

       //Game over text
       this.gameOverText = this.add.text(TextConfig.gameOver.position.x, TextConfig.gameOver.position.y, TextConfig.gameOver.text, TextConfig.gameOver.style);
       this.centralizeText(this.gameOverText);
       this.gameOverText.setDepth(DepthConfig.UI);
       this.gameOverText.setVisible(false);
    }

    //Align center for texts
    centralizeText(text: Phaser.GameObjects.Text): void {
        text.x -= text.width / 2;
        text.y -= text.height / 2;
    }

    initializePhysics(): void {
        //Interaction between shooter ball and bubbles on the board
        this.physics.add.overlap(this.shooter.ball.sprite, this.bubbleGroup.bubbleCollection, this.shooter.overlap, null, this.shooter);
        
        //Adjusted world bounds for more accurate bouncing
        this.physics.world.setBounds(-BubbleConfig.bubble.radius, 0, DEFAULT_WIDTH + BubbleConfig.bubble.diameter, DEFAULT_HEIGHT, true, true, false, false);
    
        //Interaction between shooter ball and the top of the board (score panel)
        this.physics.add.overlap(this.shooter.ball.sprite, this.scorePanel, this.shooter.overlapPanel, null, this.shooter);
    }

    initializeControl(): void {
        //Restart scene
        const r_key = this.input.keyboard.addKey('R');
        r_key.on('down', function (event) {
            this.destroyObjects();
            this.scene.restart();
        }.bind(this));

        //Shooter
        //Press mouse inside canvas
        this.input.on('pointerdown', function (pointer) {
            this.shooter.prepareShooter(pointer);
        }.bind(this));
        //Release mouse inside canvas
        this.input.on('pointerup', function (pointer) {
            this.shooter.releaseShooter(true);
        }.bind(this));
        //Release mouse outside canvas
        this.input.on('pointerupoutside', function (pointer) {
            this.shooter.releaseShooter(false);
        }.bind(this));
        this.input.on('pointermove', function (pointer) {
            if (this.shooter.touching)
            {
                this.shooter.turnShooter(pointer);
            }
        }.bind(this));

        //Restart button
        //Enlarge restart button upon hover
        this.restartButton.setInteractive();
        this.restartButton.on('pointerover', function(pointer) {
            this.restartButton.setScale(UIConfig.restart.hoverScale.x, UIConfig.restart.hoverScale.y);
        }.bind(this));

        //Shrink restart button back upon mouse out
        this.restartButton.on('pointerout', function(pointer) {
            this.restartButton.setScale(UIConfig.restart.originalScale.x, UIConfig.restart.originalScale.y);
        }.bind(this));

        //Restart scene upon pressing restart button
        this.restartButton.on('pointerdown', function(pointer) {
            this.scene.restart();
        }.bind(this));
    }

    destroyObjects(): void {
        this.input.keyboard.removeKey('R');
        this.input.removeAllListeners();
    }

    gameOver(): void {
        this.destroyObjects();
        this.gameOverPanel.setVisible(true);
        this.gameOverText.setVisible(true);
        this.restartButton.setVisible(true);
    }

    update(time, delta): void {

    }
}
import "phaser";
import Bubble from './Bubble';
import BubbleGroupConfig from '../../Config/BubbleGroupConfig';
import ShooterConfig from '../../Config/ShooterConfig';
import { DEFAULT_WIDTH } from '../../Config/Config';
import Constants from '../../Config/Constants';
import BubbleGroup from "./BubbleGroup";
import { ArrowSpriteConfig } from '../../Config/SpriteConfig';
import DepthConfig from '../../Config/DepthConfig';
import Slot from './Slot';
import AudioConfig from '../../Config/AudioConfig';

export default class Shooter {
    ball: Bubble; //Bubble to be launched
    tip: Phaser.GameObjects.Image; //Arrow tip
    touching: boolean; //Shooter state
    arrowLine: Phaser.Geom.Line; //Line for arrow body
    graphics: Phaser.GameObjects.Graphics; //To render lines
    cursor: Array<Phaser.Geom.Point>; //Array of points for cursor dots
    bubbleGroup: BubbleGroup; //BubbleGroup to which the shooter belongs
    direction: Phaser.Math.Vector2; //Direction to shoot the bubble in
    active: boolean; //State that dictates if the ball can collide with bubbles

    constructor(bubbleGroup: BubbleGroup) {
        this.bubbleGroup = bubbleGroup;
        this.initializeShooter();
    }

    initializeShooter(): void {
        //Ball
        this.ball = new Bubble(this.bubbleGroup, ShooterConfig.ball.position);

        //Enable bouncing off walls
        this.ball.sprite.setCollideWorldBounds(true);

        //Bounce does not decrease speed
        this.ball.sprite.setBounce(1, 1);
        
        //Arrow tip
        this.tip = this.bubbleGroup.scene.add.image(ShooterConfig.ball.position.x, ShooterConfig.ball.position.y, ArrowSpriteConfig.name);
        this.tip.setTint(ShooterConfig.arrow.tip.color);
        this.tip.setVisible(false);

        //Cursor
        this.cursor = [];
        for (let i = 0; i < ShooterConfig.arrow.cursor.count; i++)
        {
            this.cursor.push(new Phaser.Geom.Point(ShooterConfig.arrow.cursor.inactivePosition.x, ShooterConfig.arrow.cursor.inactivePosition.y));
        }

        //Graphics
        this.graphics = this.bubbleGroup.scene.add.graphics();
        this.graphics.setDepth(DepthConfig.cursor);
    }

    reset(): void {
        this.ball.sprite.body.reset(ShooterConfig.ball.position.x, ShooterConfig.ball.position.y);
        this.ball.sprite.setAngle(0);
        this.ball.randomizeColor();
    }

    snap(slot: Slot): void {
        this.active = false;

        //Get the nearest neighbor of the touched slot to the incoming ball
        const nearestSlot: Slot = slot.getNearestNeighbor(this.ball.sprite.body.position);

        let bubble: Bubble;
        if (this.bubbleGroup.bubblePool.length > 0)
        {
            //If there is bubble in the inactive pool, recycle it
            bubble = this.bubbleGroup.bubblePool.pop();
            bubble.reset(nearestSlot.position);
        }
        else
        {
            //Otherwise, generate new bubble to occupy the slot
            bubble = new Bubble(this.bubbleGroup, nearestSlot.position);
        }

        //Copy the ball color to the new bubble
        bubble.setColor(this.ball.color);

        //Copy the ball angle to the new bubble
        bubble.sprite.setAngle(this.ball.sprite.angle);

        //Attach bubble and slot together
        nearestSlot.assignBubble(bubble);
        
        //If ball reaches 11th row
        if (nearestSlot.position.y >= ShooterConfig.deathBoundary)
        {
            //Reset shooter ball position
            this.reset();

            //Trigger game over
            this.bubbleGroup.scene.gameOver();
            return;
        }

        //Check for popping sequence
        bubble.check();

        //If there is no pop sequence
        if (this.bubbleGroup.popCount == 0)
        {
            //Play sound effect
            this.bubbleGroup.scene.sound.play(AudioConfig.pop.name);
        }

        //Reset shooter ball position
        this.reset();
    }

    //Overlap with bubbles
    overlap(ball: Phaser.Physics.Arcade.Sprite, bubble: Phaser.Physics.Arcade.Sprite): void {
        if (this.active)
        {
            //Get the slot associated to the bubble touched by the ball
            const key: string = bubble.getData('key');
            const slot: Slot = this.bubbleGroup.slots[key];
            if (slot)
            {
                this.snap(slot);
            }
        }
    }

    //Touches top of the board (score panel)
    overlapPanel(ball: Phaser.Physics.Arcade.Sprite, panel: Phaser.GameObjects.Rectangle): void {
        if (this.active)
        {
            
        }
    }

    /*
        Prepare to shoot (mouse press down)
    */
    prepareShooter(pointer): void {
        //If there is an active ball or popping event, return
        if (this.active || this.bubbleGroup.popCount > 0)
        {
            return;
        }

        if (pointer.y > ShooterConfig.deactivateBoundary.y)
        {
            //If pointer is within the shooting boundary
            const angle = this.getAngle(pointer);

            //If angle is within the scope set in the config
            if (Math.abs(angle) < ShooterConfig.angleLimit)
            {
                //Activate touching state
                this.touching = true;

                //Update shooter
                this.turnShooter(pointer);

                //Set tip to visible
                this.tip.setVisible(true);
            }
        }
    }
    /*
        Release shooter (mouse release)
    */
    releaseShooter(shoot: boolean): void {
        //If there is an active ball or popping event, return
        if (this.active || this.bubbleGroup.popCount > 0)
        {
            return;
        }

        if (this.touching)
        {
            //Disable touching state
            this.touching = false;
            this.graphics.clear();

            //Hide tip
            this.tip.setVisible(false);

            if (shoot)
            {
                this.active = true;
                const velocity: {x: number, y: number} = {
                    x: this.direction.x * ShooterConfig.shootSpeed,
                    y: this.direction.y * ShooterConfig.shootSpeed
                };
                this.ball.sprite.setVelocity(velocity.x, velocity.y);
            }
        }
    }
    /*
        Turns the shooter based on cursor position.
    */
    turnShooter(pointer): void {
        if (pointer.y <= ShooterConfig.deactivateBoundary.y)
        {
            //If pointer is outside the shooting boundary, deactivate shooter
            this.releaseShooter(false);
            return;
        }

        const angle = this.getAngle(pointer);

        //If angle is within the scope set in the config
        if (Math.abs(angle) < ShooterConfig.angleLimit)
        {
            //Change shooter angle
            this.ball.sprite.setAngle(angle);
            this.tip.setAngle(angle);

            this.updateArrowLine(pointer);
        }
    }
    /*
        Update arrowLine graphics.
    */
    updateArrowLine(pointer): void {
        //Update arrowLine coordinates
        if (this.arrowLine)
        {
            this.arrowLine.setTo(pointer.x, pointer.y, ShooterConfig.ball.position.x, ShooterConfig.ball.position.y);
        }
        else
        {
            this.arrowLine = new Phaser.Geom.Line(pointer.x, pointer.y, ShooterConfig.ball.position.x, ShooterConfig.ball.position.y);
        }

        //Re-render arrowLine & cursor
        this.graphics.clear();
        this.graphics.lineStyle(ShooterConfig.arrow.line.width, ShooterConfig.arrow.line.color);
        this.graphics.strokeLineShape(this.arrowLine);

        //Get cursor direction
        const direction: Phaser.Math.Vector2 = new Phaser.Math.Vector2(ShooterConfig.ball.position.x - pointer.x, ShooterConfig.ball.position.y - pointer.y);
        direction.normalize();

        this.direction = direction;

        //Starting position for dots
        let dotPosition: Phaser.Math.Vector2 = new Phaser.Math.Vector2(
            //Begin from the shooter position
            ShooterConfig.ball.position.x,
            ShooterConfig.ball.position.y
        );

        //Increment for dot position
        let increment: {x: number, y: number} = {
            x: direction.x * ShooterConfig.arrow.cursor.gap,
            y: direction.y * ShooterConfig.arrow.cursor.gap
        };

        //Draw dots
        let stop: boolean = false;
        this.graphics.fillStyle(ShooterConfig.arrow.cursor.color);
        this.cursor.forEach(function (point: Phaser.Geom.Point) {
            if (stop)
            {
                return;
            }

            //Increment dot position and assign the position to the point
            dotPosition.x += increment.x;
            dotPosition.y += increment.y;
            //If cursor hits wall, then reflect off
            if (dotPosition.x <= 0 || dotPosition.x >= DEFAULT_WIDTH)
            {
                if (dotPosition.x <= 0)
                {
                    dotPosition.x *= -1;
                }
                else if (dotPosition.x >= DEFAULT_WIDTH)
                {
                    dotPosition.x = 2 * DEFAULT_WIDTH - dotPosition.x;
                }
                
                dotPosition.x = (dotPosition.x >= DEFAULT_WIDTH) ? DEFAULT_WIDTH : dotPosition.x;
                increment.x *= -1;
            }
            point.setTo(dotPosition.x, dotPosition.y);

            //Get slot that is nearest to the dot
            let y: number = point.y - BubbleGroupConfig.startingPosition.y;
            const yIndex: number = Math.round(y / BubbleGroupConfig.increment.y); 
            
            const evenOdd: number = (yIndex % 2); //Even [0] or odd [1] row

            let x: number = point.x - BubbleGroupConfig.startingPosition.x[evenOdd];
            const xIndex: number = Math.round(x / BubbleGroupConfig.increment.x);

            y = Math.floor(BubbleGroupConfig.startingPosition.y + yIndex * BubbleGroupConfig.increment.y);
            x = Math.floor(BubbleGroupConfig.startingPosition.x[evenOdd] + xIndex * BubbleGroupConfig.increment.x);

            const key = x + "|" + y;

            //If there is a slot in the dot's position
            if (this.bubbleGroup.slots[key])
            {
                //If that slot contains a bubble
                if (this.bubbleGroup.slots[key].bubble)
                {
                    //Stop propagating the cursor
                    stop = true;
                }
            }

            this.graphics.fillPointShape(point, ShooterConfig.arrow.cursor.width);
        }.bind(this));
    }
    /*
        Get angle for shooter rotation
    */
    getAngle(pointer): number {
        return -1 * Math.atan((pointer.x - ShooterConfig.ball.position.x) / (pointer.y - ShooterConfig.ball.position.y)) / Constants.DEG_TO_RAD;
    }
}
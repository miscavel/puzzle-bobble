import "phaser";
import { BubbleSpriteConfig } from '../../Config/SpriteConfig';
import BubbleConfig from '../../Config/BubbleConfig';
import Slot from './Slot';
import { BubbleAnimationList } from '../../Config/AnimationConfig';
import AudioConfig from '../../Config/AudioConfig';
import BubbleGroup from './BubbleGroup';
import ScoreConfig from '../../Config/ScoreConfig';
import DepthConfig from '../../Config/DepthConfig';

export default class Bubble {
    /*
        The bubble object that is visible to the player.
        'sprite': the sprite of the bubble.
        'slot': the slot object that contains the bubble.
        'color': the default color of the bubble.
        'bubbleGroup': the BubbleGroup to which the bubble belongs
    */
    sprite: Phaser.Physics.Arcade.Sprite;
    slot: Slot;
    color: number;
    bubbleGroup: BubbleGroup;

    constructor(bubbleGroup: BubbleGroup, position: Phaser.Math.Vector2) {
        this.bubbleGroup = bubbleGroup;
        this.instantiate(position);
    }

    instantiate(position: Phaser.Math.Vector2): void {
        const scene: Phaser.Scene = this.bubbleGroup.scene;

        //Adds the bubble to the scene's physics
        this.sprite = this.bubbleGroup.bubbleCollection.create(position.x, position.y, BubbleSpriteConfig.name);

        //Sets the bubble size so that 8 bubbles would perfectly fit a row
        this.sprite.setDisplaySize(BubbleConfig.gameObject.diameter, BubbleConfig.gameObject.diameter);
        
        //Adjusts the bubble's hitbox to perfectly match the bubble's displayed sprite
        this.sprite.body.setCircle(BubbleConfig.hitBox.radius, BubbleConfig.hitBox.offset.x, BubbleConfig.hitBox.offset.y);

        this.randomizeColor();

        //Binds a callback function for once pop animation is over
        this.sprite.on('animationcomplete', this.popCallback.bind(this), scene);
    }

    reset(position: Phaser.Math.Vector2): void {
        this.sprite.setTexture(BubbleSpriteConfig.name, 0);
        this.sprite.body.reset(position.x, position.y);
        this.sprite.setDepth(DepthConfig.bubble);
    }

    randomizeColor(): void {
        //Randomize bubble color
        this.color = BubbleConfig.colors[Phaser.Math.RND.between(0, BubbleConfig.colors.length - 1)];
        this.sprite.setTint(this.color);
    }

    setColor(color: number): void {
        this.color = color;
        this.sprite.setTint(color);
    }

    returnBubble(): void {
        //Hide bubble
        this.sprite.setPosition(BubbleConfig.inactivePosition.x, BubbleConfig.inactivePosition.y);
            
        //Returns bubble to the pool
        this.bubbleGroup.bubblePool.push(this);
    }

    //Bubble pop purposes
    assignSlot(slot: Slot): void {
        this.slot = slot;
        this.sprite.setData('key', slot.key);
    }
    check(): void {
        //Allow interaction only when there is no ongoing popping sequence
        if (this.bubbleGroup.popCount == 0)
        {
            //If bubble is associated to a slot
            if (this.slot)
            {
                this.slot.check();
            }
        }
    }
    pop(): void {
        //Plays pop animation
        this.sprite.play(BubbleAnimationList.pop);

        //Plays pop sound
        this.bubbleGroup.scene.sound.play(AudioConfig.pop.name);

        //Increase score by 'pop' amount
        this.bubbleGroup.score.addScore(ScoreConfig.pop);
    }
    popCallback(animation, frame): void {
        if (animation.key == BubbleAnimationList.pop)
        {
            //Once pop animation is over, return bubble to bubble pool
            this.returnBubble();

            //Decrement popCount as a bubble in the popping sequence has popped
            this.bubbleGroup.popCount--;

            //Callback once a pop sequence finishes
            if (this.bubbleGroup.popCount == 0)
            {
                this.bubbleGroup.traceAnchoredBubbles();
            }
        }
    }
    releaseSlot(): void {
        this.slot = undefined;
        this.sprite.setData('key', 'undefined');
    }

    //Bubble falling purposes
    fall(): void {
        //Release slot
        this.slot.bubble = undefined;
        this.releaseSlot();

        //Make bubble appear above anchored bubble
        this.sprite.setDepth(BubbleConfig.fallDepth);

        //Accelerate bubble downwards
        this.sprite.setAcceleration(BubbleConfig.fallGravity.x, BubbleConfig.fallGravity.y);

        //Distance of sprite from the bottom of the board
        const distance = BubbleConfig.fallBoundary.y - this.sprite.body.y;

        //Calculate time to reach bottom of the board in ms
        const time = Math.sqrt(2 * distance / BubbleConfig.fallGravity.y) * 1000;

        //Increase score by 'fall' amount
        this.bubbleGroup.score.addScore(ScoreConfig.fall);

        this.bubbleGroup.scene.time.addEvent({
            delay: time, //After bubble reaches bottom of the board
            callback: function () {
                //Return bubble to pool
                this.returnBubble();
            }.bind(this)
        });
    }
}
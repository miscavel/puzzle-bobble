import "phaser";
import Bubble from './Bubble';
import BubbleConfig from "../../Config/BubbleConfig";
import BubbleGroup from "./BubbleGroup";

export default class Slot {
    /*
        Refers to the space in the game board that can be filled with bubble.
        'position': the actual position of the slot in the game world (e.g. {x: 47, y: 47})
        'bubble': the bubble that is occupying the slot, in case there is none, will be set 
        to 'undefined'
        'neighbors': refer to other slots connected to this slot object (for color matching)
        'bubbleGroup': the BubbleGroup to which the slot belongs
        'key': the slot's key in the hashmap
    */
    position: Phaser.Math.Vector2;
    bubble: Bubble;
    neighbors: Array<Slot>;
    bubbleGroup: BubbleGroup;
    key: string;

    constructor(position: Phaser.Math.Vector2, bubble: Bubble, bubbleGroup: BubbleGroup, key: string) {
        this.position = position;
        this.bubbleGroup = bubbleGroup;
        this.key = key;
        this.assignBubble(bubble);
        this.neighbors = [];
    }

    assignBubble(bubble: Bubble): void {
        this.bubble = bubble;
        if (bubble)
            bubble.assignSlot(this);
    }

    registerNeighbor(slot: Slot): void {
        this.neighbors.push(slot);
    }

    //Bubble pop purposes
    checked: boolean;
    check(): void {
        //Array containing slots that have bubbles of the same color as the current slot's bubble
        let sameGroup: Array<Slot> = [];

        /*
            Array containing slots that get checked in the recursion process.
            Used to reset each slot's 'checked' flag after the recursion is done.
        */
        let checkedGroup: Array<Slot> = [];

        this.checkRecursive(this, sameGroup, checkedGroup, this.bubble.color);

        //Triggers popping if number of matching bubbles exceed set amount: 3
        if (sameGroup.length >= BubbleConfig.popCount)
        {
            //Set popCount to the number of bubbles that pop in the sequence
            this.bubbleGroup.popCount = sameGroup.length;

            let delay = 0;
            sameGroup.forEach(function (slot: Slot) {
                this.bubbleGroup.scene.time.addEvent({
                    delay: delay,
                    callback: function () {
                        //Pops bubble after delay
                        slot.pop();
                    }
                });
                slot.release();
                delay += BubbleConfig.popDelay;
            }.bind(this));
        }

        //Reset 'checked' flag for each traversed slot back to false
        checkedGroup.forEach(function (slot: Slot) {
            slot.checked = false;
        });
    }
    checkRecursive(slot: Slot, sameGroup: Array<Slot>, checkedGroup: Array<Slot>, ref: number): void {
        //If slot has been checked, skip
        if (slot.checked)
        {
            return;
        }

        //If slot has not been checked, toggle flag to true, and push to checkedGroup
        slot.checked = true;
        checkedGroup.push(slot);

        //Check if slot contains a bubble
        const bubble = slot.bubble;
        if (bubble)
        {
            //If slot's bubble has the same color as original bubble
            if (bubble.color === ref)
            {
                //Push slot to sameGroup
                sameGroup.push(slot);

                //And check slot's neighbors recursively
                slot.neighbors.forEach(function (neighbor: Slot) {
                    neighbor.checkRecursive(neighbor, sameGroup, checkedGroup, ref);
                });
            }
        }
    }
    release(): void {
        //Releases slot reference from bubble to prevent repeated check
        this.bubble.releaseSlot();
    }
    pop(): void {
        const bubble: Bubble = this.bubble;

        //Release bubble
        this.bubble = undefined;

        //Pops the associated bubble
        bubble.pop();
    }

    //Bubble falling purposes
    anchored: boolean;

    //Bubble snapping purposes
    getNearestNeighbor(position: Phaser.Math.Vector2): Slot {
        /*
            Function to get the nearest neighbor of a bubble to an incoming ball shot by the shooter
        */
        let slot: Slot;
        let minDistance: number = undefined;

        const adjustedPosition: {x: number, y: number} = {
            x: position.x + BubbleConfig.bubble.radius,
            y: position.y + BubbleConfig.bubble.radius
        }

        this.neighbors.forEach(function (neighbor: Slot) {
            //If neighbor is occupied by a bubble, skip
            if (neighbor.bubble)
            {
                return;
            }

            const distance: number = Math.pow(neighbor.position.x - adjustedPosition.x, 2.0) + Math.pow(neighbor.position.y - adjustedPosition.y, 2.0);
            if (minDistance)
            {
                if (distance < minDistance)
                {
                    minDistance = distance;
                    slot = neighbor;
                }
            }
            else
            {
                minDistance = distance;
                slot = neighbor;
            }
        });

        if (!slot)
        {
            //If all neighbors are full, get the nearest neighbor
            this.neighbors.forEach(function (neighbor: Slot) {
                const distance: number = Math.pow(neighbor.position.x - adjustedPosition.x, 2.0) + Math.pow(neighbor.position.y - adjustedPosition.y, 2.0);
                if (minDistance)
                {
                    if (distance < minDistance)
                    {
                        minDistance = distance;
                        slot = neighbor;
                    }
                }
                else
                {
                    minDistance = distance;
                    slot = neighbor;
                }
            });

            //Check the nearest neighbor for any empty slot
            slot = slot.getNearestNeighbor(position);
        }

        return slot;
    }
}